Changelog
==========

1.6.0: 2023/09/20
------------------
This version fixes important compatibility issue with numpy>=1.24.0 and matplotlib>=3.7.0.
The fix on numpy has been possible by using igor2 package instead of igor one.
Below a list of the main changes divided by modules:

* `utils.navfile`:

  * Fix for numpy>=1.24.0 by using astype(float) instead of astype(np.float);
  * Fix for numpy>=1.24.0 by using igor2 package instead of igor;
  * Because of igor2, use only one function for opening ibw or pxy files;
  * Fix loading nxarpes file when it is an hv scan and another axis is interpolated (e.g. the deflector).

* `utils.isomclass`:

  * Add default in dataprocessing and updates the doc-strings.

* `utils.fermilevel`:

  * Fix on indexes in align_to_same_eb, the data from a photon energy scan are now within the correct binding energy range.

* `navarp_gui.py`:

  * Fix Artistlist object without remove method by using only ax.cla();
  * Prevent app crash when error in file loading, and add an error message in a window box.

* `docs`:

  * Simplified installation procedure.

* `requirements.txt`:

  * Replaced igor with igor2.
  * Specified matplotlib>=3.5.2,<3.8 , because of bug in 3.5.1, and issue on patches.Polygon in 3.8.

* `setup.py`:

  * Replaced igor with igor2.
  * Specified matplotlib>=3.5.2, because of bug in 3.5.1.

* `.gitlab-ci.yml`:

  * Using python 3.10 in GitLab CI/CD.

1.5.0: 2023/04/22
------------------
This version adds the support for MBS txt-file new format and for the ANTARES new one, in addition to fix important issue.
Below a list of the main changes divided by modules:

* `utils.navfile`:

  * Add energy_scale in yaml file for info on binding energy;
  * Fix new ANTARES nxs-file: rmv deflector nan, fix new mono name, load all analyzer params;
  * Fix MBS txt-file loader including new format.

* `navarp_gui.py`:

  * Fix kxbinsSpinBox giving an int instead of float;
  * Use int instead of round because latter not always an int.

* `docs`:

  * Add install click in basic installation procedure.

1.4.0: 2022/06/21
------------------
This version adds the support for nxs-Lorea single scan, MBS krx-file single image 32bit and SpecsLab Prodigy itx-file.
Below a list of the main changes divided by modules:

* `utils.navfile`:

  * Added support for MBS-krx single image 32bit acquisition;
  * Added support for nxs-Lorea single scan;
  * Added support for SpecsLab Prodigy itx-file.

* `utils.isocut`:

  * Replaced np.sum with np.nansum to replace nan with zero in the sum.

* `docs`:

  * Endorced matplotlib=3.5.2 installation instead of 3.5.1 because of a bug in pcolormesh, almost freezing the plot.
  * Updated the copyrights.

1.3.0: 2021/12/15
------------------
This version includes the possibility to rotate the isoenergy once shown, adds the loading support for MBS-krx single image acquisition and fix the loading of single scienta txt and hv_scan once loaded with a yaml-file.
Below a list of the main changes divided by modules:

* `utils.navfile`:

  * Added support for MBS-krx single image acquisition;
  * Fix load single scienta txt file;
  * Fix loading hv scan collection of files using yaml-file.

* `utils.isomclass`:

  * Added rotate in isoenergy show method.

* `examples`:
  * Added example on isoenergy rotation in graphene deflector scan.

1.2.0: 2021/11/23
------------------
This version includes many fix, a revision of the documentations and the following new features: (i) support to the new MBS-krx file the lorea NxARPES-file; (ii) the minimum gradient method as post-processing procedure.
Below a list of the main changes divided by modules:

* `navarp_gui.py`:

  * Added post-processing procedure with order=1, which is the min minimum gradient method.

* `utils.navfile`:

  * Fixed I05 file loading;
  * Added new MBS-krx 64 bit file keeping the compatibility with the 32 bit one;
  * Added tht_an and kspace in the yaml-file for loading already transformed data.

* `utils.isomclass`:

  * Transpose data in isok export as nexus.


1.1.0: 2021/04/11
------------------

This version includes very interesting new features as: (i) exporting the analyzed data on top or bottom panels as NXdata, or Igor Pro Text file, or HD5F; (ii) post-processing procedures as second derivative (in one direction), Laplace filter (if both direction are considered), curvature (only in one direction at the moment), Gaussian filter. Below a list of the main changes divided by modules:

* `navarp_gui.py`:

  * Added post-processing procedure;
  * Added panel for exporting the data;
  * Use isomclass when possible;
  * Removed the boolean variable in the analyzer panel called deflector because useless.
  * Write efermi in the gui line edit when file loaded.

* `utils.navfile`:

  * Removed the last angle value if angle array has the same length of energies;
  * Fixed I05 file loading;
  * Removed the boolean variable in NavAnalyzer called deflector because useless.

* `utils.isomclass` (new file):

  * Fix isoenergy export as itx by transposing data.
  * Fix export as NXdata and Igor-pro text file.
  * Add postprocessing function and use it in all the isomclass.
  * Use laplace filter if sigma has two values and use curvature only for sigma with one value.
  * Rename file_path_* as file_path in all the export_as_* of the isomclass.

* `utils.kinterp`:

  * In get_isoscan, use np.nan in fill_value instead of extrapolate because it is not correct to extrapolate data.

* INTALLATION.rst:

  * The installation instruction of the master branch are now based on the PyPI package.

* UPDATE.rst:

  * The update instruction of the master branch are now based on the PyPI package.

1.0.0: 2021/04/11
------------------

This is one of the most important release of NavARP, and the project is approaching to a more stable stage. This new version adds very important methods to the NavEntry class which are nicely shown in the now-available examples gallery! In addition, the loading speed has been improved for many text-based files input, and now the krx-MBS format is also supported. Below a list of the main changes divided by modules:

* `navarp_gui.py`:

  * Added more colorscale maps;
  * After closing the program, its window size and position are saved (as a file called .navarp inside the user home directory) and they will be used to restore the program the next time it will be opened;

* `utils.navfile`:

  * Added support for krx-MBS file format;
  * Added support for ibw file format as saved by Scienta-Omicron SES program;
  * Added support for txt-Scienta file format with 3 dimensions;
  * Added support for igor-pro text file format (saved as .itx or .txt);
  * Loading speed of about x3 faster for txt-based file input (txt from Scienta and MBS, sp2 from Specs);
  * Added set_efermi method to NavEntry to set the efermi of the entry object;
  * Added autoset_efermi method to NavEntry to automatically found the efermi by curve fitting;
  * Added plt_efermi_fit method to NavEntry to show the autoset_efermi results;
  * Added set_tht_an method to NavEntry to set the tht_an angle of the the entry object for k-space tranformation;
  * Added set_kspace method to NavEntry to set tht_an, phi_an, scans_0 and phi used in the k-space tranformation;
  * Added isoscan, isoenergy, isoangle and isok methods in the NavEntry calling the respectively classes in isomclass.
  * Extended dictionary in yaml-file, if loaded overwrite any attribute and efermi and tht_an can be also directly assigned.

* `utils.isomclass` (new file):

  * Added IsoScan class with the methods to show in a plot, export as NXdata or Igor-pro text file, postprocessing as interpolation, second derivative and curvature;
  * Added IsoEnergy class  with the methods to show in a plot, export as NXdata or Igor-pro text file, postprocessing as interpolation, second derivative and curvature;
  * Added IsoAngle class with the methods to show in a plot, postprocessing as second derivative and curvature;
  * Added IsoK class with the methods to show in a plot, export as NXdata or Igor-pro text file, postprocessing as second derivative and curvature;

* `utils.fermilevel`:

  * Added fit procedure without using lmfit, lmfit is no longer required in navarp;
  * Removed all the previous functions based on lmfit.

* `utils.ktransf`:

  * Removed all the deprecated functions for k-space transformation.

* `examples directory`:

  * The old example directory now is called examples and contains the python script to get the gallery.

* `extras.simulation`:

  * Added functions to simulate the graphene band structure probed by deflector and hv scans.

* `requirements.txt and setup.py`:

  * Removed lmfit from the required packages.


0.18.0: 2020/04/11
------------------

This version gives the possibility to open four additional file formats.

* `navarp.py` (now renamed as `navarp_gui.py`):

  * Added click command;
  * Import navarp.utils, so now navarp requires installation with pip before usage;
  * Renamed as navarp_gui.py so to avoid importing conflicts.

* `utils.navfile`:

  * Added loading MBS A1Soft text-files;
  * Added case scan_type=deflector for Lorea by reading defl_angles.

* `setup.py`:

  * Added click command.


0.17.0: 2020/08/14
------------------

This version gives the possibility to open four additional file formats.

* `navarp.py`:

  * Added .navarp configuration file saved in the local user home
  * Set the initial default value of k-transf to be without photons contribution

* `utils.navfile`:

  * Added loading Scienta-Omicron SES zip and text files;
  * Added loading Specs Prodigy sp2 files;
  * Added loading Igor-pro pxt files as saved by Scienta-Omicron SES;
  * Added loading folder with txt, sp2 or pxt files using instructions in a yaml file.

* `setup.py`:

  * Improved requirements avoiding to install PyQt5 if in conda it is already present.


0.16.0: 2020/08/05
------------------

This version adds the sphinx-doc integrated in GitLab pages and utils.kinterp.

* `navarp.py`:

  * Added azimuth scan_type
  * Added arctan2 value for sample alignment
  * Fixed Antares data including the case of MBSAcquisition_1

* `utils`:

  * Added `kinterp`
