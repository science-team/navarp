.. navarp documentation master file, created by
   sphinx-quickstart on Thu Jul 30 18:29:39 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

NavARP documentation
-------------------------------

**Nav**\ igation tools for **A**\ ngle **R**\ esolved **P**\ hotoemission spectroscopy data, *i.e.*:
    * a **companion app** during ARPES data acquitision (as in beamtime); 
    * a set of **dedicated python libs** helping to get high quality figures for publication.

*By Federico Bisti, University of L'Aquila, Italy.*

.. image:: ../images/intro.gif
   :width: 100%

Table of Contents
==================================

.. toctree::
   :maxdepth: 2

   installation
   update
   usage
   auto_examples/index
   authors
   contributing
   changelog

API reference
==================
.. toctree::
   :maxdepth: 2

   utils

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Project on GitLab
==================================

- `Source repository <https://gitlab.com/fbisti/navarp>`_
- `Issue tracker <https://gitlab.com/fbisti/navarp/-/issues>`_
