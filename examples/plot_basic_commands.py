# -*- coding: utf-8 -*-
"""
Simulated cone basic analysis
=============================

Simple workflow for analyzing a deflector scan data.
The same workflow can be applied in the case of manipulator angular scans.
"""

# %%
# Import the "fundamental" python libraries for a generic data analysis:

import numpy as np

# %%
# Import the navarp libraries:

from navarp.utils import navfile

# %%
# Load the data from a file:

file_name = r"nxarpes_simulated_cone.nxs"
entry = navfile.load(file_name)

# %%
# Plot a single slice
# Ex: scan = 0.5

scan = 0.5
entry.isoscan(scan).show()

# %%
# Fermi level determination

entry.autoset_efermi(energy_range=[93.8, 94.3])
entry.plt_efermi_fit()

print("Fermi level = {:.3f} meV".format(entry.efermi))
print("Energy resolution = {:.0f} meV".format(entry.efermi_fwhm*1000))
print("hv = {:g} eV".format(np.squeeze(entry.hv)))

# %%
# Since now the Fermi level is known, the same plot is automatically aligned
# Ex: scan = 0.5

scan = 0.5
entry.isoscan(scan).show()

# %%
# Plotting iso-energetic cut
# Ex: isoenergy cut at ekin = efermi

ebin = 0
debin = 0.005
entry.isoenergy(ebin, debin).show()
